-- Gaspard VANDECASTEELE
-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : Dim 28 nov. 2021 à 15:16
-- Version du serveur :  10.5.8-MariaDB
-- Version de PHP : 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_flight`
--
CREATE DATABASE IF NOT EXISTS `db_flight` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `db_flight`;

-- --------------------------------------------------------

--
-- Structure de la table `aircrafts`
--

CREATE TABLE `aircrafts` (
  `AIRCRAFT_NBR` smallint(6) NOT NULL,
  `LAST_SERVICING` date DEFAULT NULL,
  `TOTALFLIGHTHOURS` int(11) DEFAULT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `aircrafts`:
--   `IATA`
--       `koaircraft` -> `IATA`
--

--
-- Déchargement des données de la table `aircrafts`
--

INSERT INTO `aircrafts` (`AIRCRAFT_NBR`, `LAST_SERVICING`, `TOTALFLIGHTHOURS`, `IATA`) VALUES
(1, '2021-11-25', 84000, 'AF32'),
(2, '2021-11-23', 132000, 'HE54');

-- --------------------------------------------------------

--
-- Structure de la table `airlinks`
--

CREATE TABLE `airlinks` (
  `AIRLINK_CODE` int(11) NOT NULL,
  `AIRLINK_DISTANCE` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AIRPORT_CODE_FROM` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AIRPORT_CODE_TO` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `airlinks`:
--   `AIRPORT_CODE_FROM`
--       `airports` -> `AIRPORT_CODE`
--   `AIRPORT_CODE_TO`
--       `airports` -> `AIRPORT_CODE`
--

--
-- Déchargement des données de la table `airlinks`
--

INSERT INTO `airlinks` (`AIRLINK_CODE`, `AIRLINK_DISTANCE`, `AIRPORT_CODE_FROM`, `AIRPORT_CODE_TO`) VALUES
(1, '25470', 'PACH', 'EJHD'),
(2, '54210', 'JPEI', 'EJHD'),
(3, '25470', 'EJHD', 'PACH');

-- --------------------------------------------------------

--
-- Structure de la table `airports`
--

CREATE TABLE `airports` (
  `AIRPORT_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CURR_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `airports`:
--   `CURR_CODE`
--       `currencies` -> `CURR_CODE`
--

--
-- Déchargement des données de la table `airports`
--

INSERT INTO `airports` (`AIRPORT_CODE`, `CURR_CODE`) VALUES
('PACH', 'EUR'),
('EJHD', 'USD'),
('JPEI', 'YEN');

-- --------------------------------------------------------

--
-- Structure de la table `assignments`
--

CREATE TABLE `assignments` (
  `ASSIGNT_NBR` int(11) NOT NULL,
  `STARTING_DATE` date DEFAULT NULL,
  `ENDING_DATE` date DEFAULT NULL,
  `STAFF_CODE` int(11) NOT NULL,
  `ROLE_CODE` int(11) NOT NULL,
  `CREW_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `assignments`:
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--   `ROLE_CODE`
--       `roles` -> `ROLE_CODE`
--   `CREW_CODE`
--       `crews` -> `CREW_CODE`
--

--
-- Déchargement des données de la table `assignments`
--

INSERT INTO `assignments` (`ASSIGNT_NBR`, `STARTING_DATE`, `ENDING_DATE`, `STAFF_CODE`, `ROLE_CODE`, `CREW_CODE`) VALUES
(1, '2019-11-13', '2019-12-20', 2, 1, 1),
(2, '2021-05-19', '2021-11-27', 3, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `checkups`
--

CREATE TABLE `checkups` (
  `CHECKUP_NBR` int(11) NOT NULL,
  `CHECKUP_DATE` date DEFAULT NULL,
  `STAFF_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `checkups`:
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--

--
-- Déchargement des données de la table `checkups`
--

INSERT INTO `checkups` (`CHECKUP_NBR`, `CHECKUP_DATE`, `STAFF_CODE`) VALUES
(1, '2018-11-08', 1),
(2, '2021-11-18', 2),
(3, '2020-11-11', 3);

-- --------------------------------------------------------

--
-- Structure de la table `cities`
--

CREATE TABLE `cities` (
  `CITY_CODE` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CITY_GPS` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `cities`:
--

--
-- Déchargement des données de la table `cities`
--

INSERT INTO `cities` (`CITY_CODE`, `CITY_GPS`) VALUES
('PARIS', '1,1,1, 2,2,2'),
('SF', '4,5,5 43,2,4');

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `CLASS_CODE` int(11) NOT NULL,
  `CLASS_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `classes`:
--

--
-- Déchargement des données de la table `classes`
--

INSERT INTO `classes` (`CLASS_CODE`, `CLASS_NAME`) VALUES
(1, 'Premier'),
(2, 'Pro'),
(3, 'Eco');

-- --------------------------------------------------------

--
-- Structure de la table `crews`
--

CREATE TABLE `crews` (
  `CREW_CODE` int(11) NOT NULL,
  `CREW_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `crews`:
--

--
-- Déchargement des données de la table `crews`
--

INSERT INTO `crews` (`CREW_CODE`, `CREW_NAME`) VALUES
(1, 'CREWNAME1'),
(2, 'CREWNAME2');

-- --------------------------------------------------------

--
-- Structure de la table `currencies`
--

CREATE TABLE `currencies` (
  `CURR_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CURR_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `currencies`:
--

--
-- Déchargement des données de la table `currencies`
--

INSERT INTO `currencies` (`CURR_CODE`, `CURR_NAME`) VALUES
('EUR', 'Euros'),
('USD', 'US Dollar'),
('YEN', 'Yen');

-- --------------------------------------------------------

--
-- Structure de la table `day_periods`
--

CREATE TABLE `day_periods` (
  `DAY_PERIOD` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MONTH_PERIOD` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DAY_NBR` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `day_periods`:
--   `MONTH_PERIOD`
--       `month_periods` -> `MONTH_PERIOD`
--   `DAY_NBR`
--       `weekdays` -> `DAY_NBR`
--

--
-- Déchargement des données de la table `day_periods`
--

INSERT INTO `day_periods` (`DAY_PERIOD`, `MONTH_PERIOD`, `DAY_NBR`) VALUES
('1', '1', 3),
('2', '1', 4),
('3', '1', 5),
('4', '3', 6);

-- --------------------------------------------------------

--
-- Structure de la table `doc`
--

CREATE TABLE `doc` (
  `DOC_CODE` int(11) NOT NULL,
  `DOC_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DOC_DATE` date DEFAULT NULL,
  `DOC_LINK` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `doc`:
--

--
-- Déchargement des données de la table `doc`
--

INSERT INTO `doc` (`DOC_CODE`, `DOC_NAME`, `DOC_DATE`, `DOC_LINK`) VALUES
(1, 'Document test', '2021-11-10', 'http://exemple.com'),
(2, 'Autre document', '2021-05-11', 'https://google.com');

-- --------------------------------------------------------

--
-- Structure de la table `eff_flights`
--

CREATE TABLE `eff_flights` (
  `EFF_FLIGHTS_PK` int(11) NOT NULL,
  `PLAN_DEP_DATE` time DEFAULT NULL,
  `RES_SEATS_QTY` smallint(6) DEFAULT NULL,
  `OCC_SEATS_QTY` smallint(6) DEFAULT NULL,
  `REAL_DEP_TIME` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `REAL_ARR_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LOADED_FUEL` int(11) DEFAULT NULL,
  `CREW_CODE` int(11) DEFAULT NULL,
  `AIRCRAFT_NBR` smallint(6) DEFAULT NULL,
  `PLAN_DAYFLIGHTS_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `eff_flights`:
--   `CREW_CODE`
--       `crews` -> `CREW_CODE`
--   `AIRCRAFT_NBR`
--       `aircrafts` -> `AIRCRAFT_NBR`
--   `PLAN_DAYFLIGHTS_FK`
--       `plan_dayflights` -> `PLAN_DAYFLIGHTS_PK`
--

--
-- Déchargement des données de la table `eff_flights`
--

INSERT INTO `eff_flights` (`EFF_FLIGHTS_PK`, `PLAN_DEP_DATE`, `RES_SEATS_QTY`, `OCC_SEATS_QTY`, `REAL_DEP_TIME`, `REAL_ARR_TIME`, `LOADED_FUEL`, `CREW_CODE`, `AIRCRAFT_NBR`, `PLAN_DAYFLIGHTS_FK`) VALUES
(1, '00:02:33', 68, 67, '2021-11-26 13:05:33', '0000-00-00 00:00:00', 120, 1, 1, 1),
(2, '99:06:33', NULL, NULL, '2021-11-26 13:07:14', '0000-00-00 00:00:00', NULL, NULL, NULL, 2),
(3, '07:37:00', 54, 54, '2021-09-16 16:46:33', '2021-09-17 16:46:33', 120, 2, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `eff_stopovers`
--

CREATE TABLE `eff_stopovers` (
  `ESO_NBR` smallint(6) NOT NULL,
  `REAL_TRAVEL_DUR` time DEFAULT NULL,
  `REAL_LANDING_TIME` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `REAL_TAKEOFF_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LOADED_FUEL` int(11) DEFAULT NULL,
  `STOPOVER_NBR` smallint(6) NOT NULL,
  `EFF_FLIGHTS_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `eff_stopovers`:
--   `STOPOVER_NBR`
--       `plan_stopovers` -> `STOPOVER_NBR`
--   `EFF_FLIGHTS_FK`
--       `eff_flights` -> `EFF_FLIGHTS_PK`
--

--
-- Déchargement des données de la table `eff_stopovers`
--

INSERT INTO `eff_stopovers` (`ESO_NBR`, `REAL_TRAVEL_DUR`, `REAL_LANDING_TIME`, `REAL_TAKEOFF_TIME`, `LOADED_FUEL`, `STOPOVER_NBR`, `EFF_FLIGHTS_FK`) VALUES
(4585, '13:00:00', '2021-11-26 13:20:08', '0000-00-00 00:00:00', 320, 1, 2),
(4586, '07:00:00', '2021-11-26 13:20:08', '0000-00-00 00:00:00', 270, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `EVENT_NBR` int(11) NOT NULL,
  `EVENT_LABEL` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EVENT_DSCR` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EVENT_DATE` date DEFAULT NULL,
  `EVENT_TIME` time DEFAULT NULL,
  `KoEVT_CODE` int(11) NOT NULL,
  `STAFF_CODE` int(11) NOT NULL,
  `STATUS_CODE` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `events`:
--   `KoEVT_CODE`
--       `koevent` -> `KoEVT_CODE`
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--   `STATUS_CODE`
--       `status` -> `STATUS_CODE`
--

--
-- Déchargement des données de la table `events`
--

INSERT INTO `events` (`EVENT_NBR`, `EVENT_LABEL`, `EVENT_DSCR`, `EVENT_DATE`, `EVENT_TIME`, `KoEVT_CODE`, `STAFF_CODE`, `STATUS_CODE`) VALUES
(1, 'Panne moteur', 'Panne moteur 1 AF123', '2021-09-18', '24:51:14', 1, 2, 1),
(3, 'Stick shaker', 'Stick Shaker stall event', '2017-07-17', '14:10:10', 2, 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `koaircraft`
--

CREATE TABLE `koaircraft` (
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTONOMY` int(11) DEFAULT NULL,
  `SPEED` int(11) DEFAULT NULL,
  `SEATS` int(11) DEFAULT NULL,
  `MAX_LOAD` int(11) DEFAULT NULL,
  `Max_FUEL` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `koaircraft`:
--

--
-- Déchargement des données de la table `koaircraft`
--

INSERT INTO `koaircraft` (`IATA`, `AUTONOMY`, `SPEED`, `SEATS`, `MAX_LOAD`, `Max_FUEL`) VALUES
('AF32', 30000, 300, 60, 30, 120),
('HE54', 450000, 275, 80, 60, 360);

-- --------------------------------------------------------

--
-- Structure de la table `kocrew`
--

CREATE TABLE `kocrew` (
  `KoCREW_CODE` int(11) NOT NULL,
  `KoCREW_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `kocrew`:
--

--
-- Déchargement des données de la table `kocrew`
--

INSERT INTO `kocrew` (`KoCREW_CODE`, `KoCREW_NAME`) VALUES
(1, 'CREW1'),
(2, 'CREW2');

-- --------------------------------------------------------

--
-- Structure de la table `koevent`
--

CREATE TABLE `koevent` (
  `KoEVT_CODE` int(11) NOT NULL,
  `KoEVT_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `koevent`:
--

--
-- Déchargement des données de la table `koevent`
--

INSERT INTO `koevent` (`KoEVT_CODE`, `KoEVT_NAME`) VALUES
(1, 'KoEVENT 1'),
(2, 'KoEVENT 2');

-- --------------------------------------------------------

--
-- Structure de la table `kolicense`
--

CREATE TABLE `kolicense` (
  `KoLIC_CODE` int(11) NOT NULL,
  `KoLIC_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `kolicense`:
--

--
-- Déchargement des données de la table `kolicense`
--

INSERT INTO `kolicense` (`KoLIC_CODE`, `KoLIC_NAME`) VALUES
(1, 'Pilot License'),
(2, 'Manager License'),
(3, 'FO License');

-- --------------------------------------------------------

--
-- Structure de la table `languages`
--

CREATE TABLE `languages` (
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `languages`:
--

--
-- Déchargement des données de la table `languages`
--

INSERT INTO `languages` (`LNG_CODE`) VALUES
('EN'),
('FR'),
('GE');

-- --------------------------------------------------------

--
-- Structure de la table `month_periods`
--

CREATE TABLE `month_periods` (
  `MONTH_PERIOD` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `YEAR_PERIOD` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `month_periods`:
--   `YEAR_PERIOD`
--       `year_periods` -> `YEAR_PERIOD`
--

--
-- Déchargement des données de la table `month_periods`
--

INSERT INTO `month_periods` (`MONTH_PERIOD`, `YEAR_PERIOD`) VALUES
('1', '2019'),
('1', '2020'),
('1', '2021'),
('1', '2022'),
('10', '2019'),
('10', '2020'),
('10', '2021'),
('10', '2022'),
('11', '2019'),
('11', '2020'),
('11', '2021'),
('11', '2022'),
('12', '2019'),
('12', '2020'),
('12', '2021'),
('12', '2022'),
('2', '2019'),
('2', '2020'),
('2', '2021'),
('2', '2022'),
('3', '2019'),
('3', '2020'),
('3', '2021'),
('3', '2022'),
('4', '2019'),
('4', '2020'),
('4', '2021'),
('4', '2022'),
('5', '2019'),
('5', '2020'),
('5', '2021'),
('5', '2022'),
('6', '2019'),
('6', '2020'),
('6', '2021'),
('6', '2022'),
('7', '2019'),
('7', '2020'),
('7', '2021'),
('7', '2022'),
('8', '2019'),
('8', '2020'),
('8', '2021'),
('8', '2022'),
('9', '2019'),
('9', '2020'),
('9', '2021'),
('9', '2022');

-- --------------------------------------------------------

--
-- Structure de la table `passengers`
--

CREATE TABLE `passengers` (
  `PSGR_CODE` int(11) NOT NULL,
  `LNAME` time DEFAULT NULL,
  `FNAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `passengers`:
--

--
-- Déchargement des données de la table `passengers`
--

INSERT INTO `passengers` (`PSGR_CODE`, `LNAME`, `FNAME`) VALUES
(1, '11:38:12', 'Passengers1'),
(2, '13:13:13', 'Passengers2');

-- --------------------------------------------------------

--
-- Structure de la table `plan_dayflights`
--

CREATE TABLE `plan_dayflights` (
  `PLAN_DAYFLIGHTS_PK` int(11) NOT NULL,
  `PLAN_SEATS` smallint(6) DEFAULT NULL,
  `PLAN_DURATION` time DEFAULT NULL,
  `PLAN_ARR_TIME` time DEFAULT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FLIGHT_NBR` smallint(6) NOT NULL,
  `DAY_NBR` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `plan_dayflights`:
--   `IATA`
--       `koaircraft` -> `IATA`
--   `FLIGHT_NBR`
--       `plan_flights` -> `FLIGHT_NBR`
--   `DAY_NBR`
--       `weekdays` -> `DAY_NBR`
--

--
-- Déchargement des données de la table `plan_dayflights`
--

INSERT INTO `plan_dayflights` (`PLAN_DAYFLIGHTS_PK`, `PLAN_SEATS`, `PLAN_DURATION`, `PLAN_ARR_TIME`, `IATA`, `FLIGHT_NBR`, `DAY_NBR`) VALUES
(1, 50, '16:00:00', '11:00:00', 'AF32', 1, 4),
(2, 70, '20:00:00', '21:00:00', 'HE54', 2, 7),
(3, 60, '06:25:25', '18:45:00', 'AF32', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `plan_flights`
--

CREATE TABLE `plan_flights` (
  `FLIGHT_NBR` smallint(6) NOT NULL,
  `DEPARTURE` time DEFAULT NULL,
  `FREQUENCY` smallint(6) DEFAULT NULL,
  `AIRLINK_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `plan_flights`:
--   `AIRLINK_CODE`
--       `airlinks` -> `AIRLINK_CODE`
--

--
-- Déchargement des données de la table `plan_flights`
--

INSERT INTO `plan_flights` (`FLIGHT_NBR`, `DEPARTURE`, `FREQUENCY`, `AIRLINK_CODE`) VALUES
(1, '00:50:00', 2340, 1),
(2, '14:10:00', 6500, 3),
(3, '00:44:39', 555, 3);

-- --------------------------------------------------------

--
-- Structure de la table `plan_stopovers`
--

CREATE TABLE `plan_stopovers` (
  `STOPOVER_NBR` smallint(6) NOT NULL,
  `Duration` time DEFAULT NULL,
  `ROUTE_CODE` int(11) DEFAULT NULL,
  `PLAN_DAYFLIGHTS_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `plan_stopovers`:
--   `ROUTE_CODE`
--       `routes` -> `ROUTE_CODE`
--   `PLAN_DAYFLIGHTS_FK`
--       `plan_dayflights` -> `PLAN_DAYFLIGHTS_PK`
--

--
-- Déchargement des données de la table `plan_stopovers`
--

INSERT INTO `plan_stopovers` (`STOPOVER_NBR`, `Duration`, `ROUTE_CODE`, `PLAN_DAYFLIGHTS_FK`) VALUES
(1, '14:00:00', 2, 2),
(2, '10:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `qualifications`
--

CREATE TABLE `qualifications` (
  `QUALIF_CODE` int(11) NOT NULL,
  `QUALIF_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `QUALIF_DSCR` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `qualifications`:
--

--
-- Déchargement des données de la table `qualifications`
--

INSERT INTO `qualifications` (`QUALIF_CODE`, `QUALIF_NAME`, `QUALIF_DSCR`) VALUES
(1, 'B737 PILOT QUALIF', 'Boeing 737 Pilot Qualification'),
(2, 'B737 FO QUALIF', 'Boeing 737 First Officer Qualification'),
(3, 'A320 PILOT QUALIF', 'Airbus A320 Pilot Qualification'),
(4, 'A320 FO QUALIF', 'Airbus A320 FO Qualification'),
(5, 'UNKNOW', '');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `ROLE_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `roles`:
--

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`ROLE_CODE`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Structure de la table `routes`
--

CREATE TABLE `routes` (
  `ROUTE_CODE` int(11) NOT NULL,
  `ROUTE_DURATION` time DEFAULT NULL,
  `AIRLINK_CODE` int(11) NOT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `routes`:
--   `AIRLINK_CODE`
--       `airlinks` -> `AIRLINK_CODE`
--   `IATA`
--       `koaircraft` -> `IATA`
--

--
-- Déchargement des données de la table `routes`
--

INSERT INTO `routes` (`ROUTE_CODE`, `ROUTE_DURATION`, `AIRLINK_CODE`, `IATA`) VALUES
(1, '06:00:00', 3, 'AF32'),
(2, '06:45:00', 1, 'HE54'),
(3, '14:30:00', 2, 'HE54');

-- --------------------------------------------------------

--
-- Structure de la table `r_accept`
--

CREATE TABLE `r_accept` (
  `AIRPORT_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_accept`:
--   `AIRPORT_CODE`
--       `airports` -> `AIRPORT_CODE`
--   `IATA`
--       `koaircraft` -> `IATA`
--

--
-- Déchargement des données de la table `r_accept`
--

INSERT INTO `r_accept` (`AIRPORT_CODE`, `IATA`) VALUES
('EJHD', 'AF32'),
('JPEI', 'HE54'),
('PACH', 'AF32'),
('PACH', 'HE54');

-- --------------------------------------------------------

--
-- Structure de la table `r_allow_pilot`
--

CREATE TABLE `r_allow_pilot` (
  `KoLIC_CODE` int(11) NOT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_allow_pilot`:
--   `KoLIC_CODE`
--       `kolicense` -> `KoLIC_CODE`
--   `IATA`
--       `koaircraft` -> `IATA`
--

--
-- Déchargement des données de la table `r_allow_pilot`
--

INSERT INTO `r_allow_pilot` (`KoLIC_CODE`, `IATA`) VALUES
(1, 'AF32'),
(1, 'HE54'),
(3, 'AF32');

-- --------------------------------------------------------

--
-- Structure de la table `r_bind`
--

CREATE TABLE `r_bind` (
  `EVENT_NBR` int(11) NOT NULL,
  `DOC_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_bind`:
--   `EVENT_NBR`
--       `events` -> `EVENT_NBR`
--   `DOC_CODE`
--       `doc` -> `DOC_CODE`
--

--
-- Déchargement des données de la table `r_bind`
--

INSERT INTO `r_bind` (`EVENT_NBR`, `DOC_CODE`) VALUES
(1, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `r_contain`
--

CREATE TABLE `r_contain` (
  `CLASS_CODE` int(11) NOT NULL,
  `AIRCRAFT_NBR` smallint(6) NOT NULL,
  `SEATS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_contain`:
--   `CLASS_CODE`
--       `classes` -> `CLASS_CODE`
--   `AIRCRAFT_NBR`
--       `aircrafts` -> `AIRCRAFT_NBR`
--

--
-- Déchargement des données de la table `r_contain`
--

INSERT INTO `r_contain` (`CLASS_CODE`, `AIRCRAFT_NBR`, `SEATS`) VALUES
(1, 1, 10),
(2, 1, 20),
(3, 1, 30),
(3, 2, 70);

-- --------------------------------------------------------

--
-- Structure de la table `r_estimate_nb_passengers`
--

CREATE TABLE `r_estimate_nb_passengers` (
  `TIMESLOT_CODE` int(11) NOT NULL,
  `AIRLINK_CODE` int(11) NOT NULL,
  `DAY_NBR` smallint(6) NOT NULL,
  `ESTIMATED_SEATS` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_estimate_nb_passengers`:
--   `TIMESLOT_CODE`
--       `timeslots` -> `TIMESLOT_CODE`
--   `AIRLINK_CODE`
--       `airlinks` -> `AIRLINK_CODE`
--   `DAY_NBR`
--       `weekdays` -> `DAY_NBR`
--

--
-- Déchargement des données de la table `r_estimate_nb_passengers`
--

INSERT INTO `r_estimate_nb_passengers` (`TIMESLOT_CODE`, `AIRLINK_CODE`, `DAY_NBR`, `ESTIMATED_SEATS`) VALUES
(1, 1, 4, 50),
(2, 2, 1, 70);

-- --------------------------------------------------------

--
-- Structure de la table `r_estimate_price`
--

CREATE TABLE `r_estimate_price` (
  `CLASS_CODE` int(11) NOT NULL,
  `CURR_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PLAN_DAYFLIGHTS_FK` int(11) NOT NULL,
  `ESTIMATED_PRICE` decimal(19,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_estimate_price`:
--   `CLASS_CODE`
--       `classes` -> `CLASS_CODE`
--   `CURR_CODE`
--       `currencies` -> `CURR_CODE`
--   `PLAN_DAYFLIGHTS_FK`
--       `plan_dayflights` -> `PLAN_DAYFLIGHTS_PK`
--

--
-- Déchargement des données de la table `r_estimate_price`
--

INSERT INTO `r_estimate_price` (`CLASS_CODE`, `CURR_CODE`, `PLAN_DAYFLIGHTS_FK`, `ESTIMATED_PRICE`) VALUES
(1, 'EUR', 1, '3600.0000'),
(2, 'EUR', 1, '3200.0000'),
(3, 'EUR', 1, '1200.0000'),
(3, 'USD', 2, '1300.0000');

-- --------------------------------------------------------

--
-- Structure de la table `r_have_qualif`
--

CREATE TABLE `r_have_qualif` (
  `QUALIF_CODE` int(11) NOT NULL,
  `STAFF_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_have_qualif`:
--   `QUALIF_CODE`
--       `qualifications` -> `QUALIF_CODE`
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--

--
-- Déchargement des données de la table `r_have_qualif`
--

INSERT INTO `r_have_qualif` (`QUALIF_CODE`, `STAFF_CODE`) VALUES
(2, 3),
(3, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `r_include`
--

CREATE TABLE `r_include` (
  `KoCREW_CODE` int(11) NOT NULL,
  `ROLE_CODE` int(11) NOT NULL,
  `NBR_IN_ROLE` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_include`:
--   `KoCREW_CODE`
--       `kocrew` -> `KoCREW_CODE`
--   `ROLE_CODE`
--       `roles` -> `ROLE_CODE`
--

--
-- Déchargement des données de la table `r_include`
--

INSERT INTO `r_include` (`KoCREW_CODE`, `ROLE_CODE`, `NBR_IN_ROLE`) VALUES
(1, 1, 6),
(2, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `r_in_flight`
--

CREATE TABLE `r_in_flight` (
  `STAFF_CODE` int(11) NOT NULL,
  `MONTH_PERIOD` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `YEAR_PERIOD` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HOURS_NBR` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_in_flight`:
--   `MONTH_PERIOD`
--       `month_periods` -> `MONTH_PERIOD`
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--   `YEAR_PERIOD`
--       `month_periods` -> `YEAR_PERIOD`
--

--
-- Déchargement des données de la table `r_in_flight`
--

INSERT INTO `r_in_flight` (`STAFF_CODE`, `MONTH_PERIOD`, `YEAR_PERIOD`, `HOURS_NBR`) VALUES
(2, '1', '2020', 54),
(2, '1', '2021', 58),
(2, '10', '2020', 136),
(2, '11', '2020', 68),
(2, '12', '2020', 32),
(2, '2', '2020', 78),
(2, '2', '2021', 74),
(2, '3', '2020', 98),
(2, '4', '2020', 102),
(2, '5', '2020', 88),
(2, '6', '2020', 115),
(2, '7', '2020', 102),
(2, '8', '2020', 122),
(2, '9', '2020', 106),
(3, '1', '2020', 124),
(3, '1', '2021', 84),
(3, '10', '2020', 124),
(3, '11', '2020', 100),
(3, '12', '2020', 30),
(3, '2', '2020', 87),
(3, '2', '2021', 68),
(3, '3', '2020', 78),
(3, '4', '2020', 114),
(3, '5', '2020', 120),
(3, '6', '2020', 103),
(3, '7', '2020', 42),
(3, '8', '2020', 89),
(3, '9', '2020', 65);

-- --------------------------------------------------------

--
-- Structure de la table `r_isa_kocrew`
--

CREATE TABLE `r_isa_kocrew` (
  `CREW_CODE` int(11) NOT NULL,
  `KoCREW_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_isa_kocrew`:
--   `CREW_CODE`
--       `crews` -> `CREW_CODE`
--   `KoCREW_CODE`
--       `kocrew` -> `KoCREW_CODE`
--

--
-- Déchargement des données de la table `r_isa_kocrew`
--

INSERT INTO `r_isa_kocrew` (`CREW_CODE`, `KoCREW_CODE`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `r_may_have`
--

CREATE TABLE `r_may_have` (
  `STAFF_CODE` int(11) NOT NULL,
  `ROLE_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_may_have`:
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--   `ROLE_CODE`
--       `roles` -> `ROLE_CODE`
--

--
-- Déchargement des données de la table `r_may_have`
--

INSERT INTO `r_may_have` (`STAFF_CODE`, `ROLE_CODE`) VALUES
(1, 1),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `r_name_airport`
--

CREATE TABLE `r_name_airport` (
  `AIRPORT_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ARP_NAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_airport`:
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--   `AIRPORT_CODE`
--       `airports` -> `AIRPORT_CODE`
--

--
-- Déchargement des données de la table `r_name_airport`
--

INSERT INTO `r_name_airport` (`AIRPORT_CODE`, `LNG_CODE`, `ARP_NAME`) VALUES
('EJHD', 'EN', 'San Francisco International Airport'),
('EJHD', 'FR', 'Aéroport international de San Francisco'),
('JPEI', 'EN', 'Japan Tokyo Aiport'),
('PACH', 'EN', 'Paris charles de gaulle'),
('PACH', 'FR', 'Paris charles de gaulle');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_city`
--

CREATE TABLE `r_name_city` (
  `CITY_CODE` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CITYNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_city`:
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--   `CITY_CODE`
--       `cities` -> `CITY_CODE`
--

--
-- Déchargement des données de la table `r_name_city`
--

INSERT INTO `r_name_city` (`CITY_CODE`, `LNG_CODE`, `CITYNAME`) VALUES
('PARIS', 'EN', 'Paris'),
('PARIS', 'FR', 'Paris'),
('SF', 'EN', 'San Francisco'),
('SF', 'FR', 'San Francisco');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_day`
--

CREATE TABLE `r_name_day` (
  `DAY_NBR` smallint(6) NOT NULL,
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DAYNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_day`:
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--   `DAY_NBR`
--       `weekdays` -> `DAY_NBR`
--

--
-- Déchargement des données de la table `r_name_day`
--

INSERT INTO `r_name_day` (`DAY_NBR`, `LNG_CODE`, `DAYNAME`) VALUES
(1, 'EN', 'Monday'),
(1, 'FR', 'Lundi'),
(2, 'EN', 'Tuesday'),
(2, 'FR', 'Mardi'),
(3, 'EN', 'Wednesday '),
(3, 'FR', 'Mercredi'),
(4, 'EN', 'Thursday'),
(4, 'FR', 'Jeudi'),
(5, 'EN', 'Friday'),
(5, 'FR', 'Vendredi'),
(6, 'EN', 'Saturday'),
(6, 'FR', 'Samedi'),
(7, 'EN', 'Sunday'),
(7, 'FR', 'Dimanche');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_koaircraft`
--

CREATE TABLE `r_name_koaircraft` (
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `KoACRNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_koaircraft`:
--   `IATA`
--       `koaircraft` -> `IATA`
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--

--
-- Déchargement des données de la table `r_name_koaircraft`
--

INSERT INTO `r_name_koaircraft` (`LNG_CODE`, `IATA`, `KoACRNAME`) VALUES
('EN', 'AF32', 'Commercial Aircraft'),
('EN', 'HE54', 'Transport Aircraft'),
('FR', 'AF32', 'Avion Commercial'),
('FR', 'HE54', 'Avion de transport');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_language`
--

CREATE TABLE `r_name_language` (
  `LNG_CODE_IN` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LNG_CODE_OF` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LNGNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_language`:
--   `LNG_CODE_IN`
--       `languages` -> `LNG_CODE`
--   `LNG_CODE_OF`
--       `languages` -> `LNG_CODE`
--

--
-- Déchargement des données de la table `r_name_language`
--

INSERT INTO `r_name_language` (`LNG_CODE_IN`, `LNG_CODE_OF`, `LNGNAME`) VALUES
('EN', 'EN', 'English'),
('EN', 'FR', 'French'),
('FR', 'EN', 'Anglais'),
('FR', 'FR', 'Français');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_month`
--

CREATE TABLE `r_name_month` (
  `MONTH_PERIOD` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MONTHNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_month`:
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--   `MONTH_PERIOD`
--       `month_periods` -> `MONTH_PERIOD`
--

--
-- Déchargement des données de la table `r_name_month`
--

INSERT INTO `r_name_month` (`MONTH_PERIOD`, `LNG_CODE`, `MONTHNAME`) VALUES
('1', 'FR', 'Janvier'),
('2', 'FR', 'Fevrier'),
('3', 'FR', 'Mars');

-- --------------------------------------------------------

--
-- Structure de la table `r_name_role`
--

CREATE TABLE `r_name_role` (
  `ROLE_CODE` int(11) NOT NULL,
  `LNG_CODE` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ROLE_NAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ROLE_DSCR` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_name_role`:
--   `ROLE_CODE`
--       `roles` -> `ROLE_CODE`
--   `LNG_CODE`
--       `languages` -> `LNG_CODE`
--

--
-- Déchargement des données de la table `r_name_role`
--

INSERT INTO `r_name_role` (`ROLE_CODE`, `LNG_CODE`, `ROLE_NAME`, `ROLE_DSCR`) VALUES
(1, 'EN', 'Numero 1 Role', 'Commercial Aircraft Role'),
(1, 'FR', 'Role numéro 1', 'Role avion commercial'),
(2, 'FR', 'Role Transport', 'Role transport');

-- --------------------------------------------------------

--
-- Structure de la table `r_need`
--

CREATE TABLE `r_need` (
  `IATA` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `KoCREW_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_need`:
--   `IATA`
--       `koaircraft` -> `IATA`
--   `KoCREW_CODE`
--       `kocrew` -> `KoCREW_CODE`
--

--
-- Déchargement des données de la table `r_need`
--

INSERT INTO `r_need` (`IATA`, `KoCREW_CODE`) VALUES
('AF32', 1),
('HE54', 2);

-- --------------------------------------------------------

--
-- Structure de la table `r_obtain`
--

CREATE TABLE `r_obtain` (
  `KoLIC_CODE` int(11) NOT NULL,
  `STAFF_CODE` int(11) NOT NULL,
  `OBTAINED` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_obtain`:
--   `KoLIC_CODE`
--       `kolicense` -> `KoLIC_CODE`
--   `STAFF_CODE`
--       `staff` -> `STAFF_CODE`
--

--
-- Déchargement des données de la table `r_obtain`
--

INSERT INTO `r_obtain` (`KoLIC_CODE`, `STAFF_CODE`, `OBTAINED`) VALUES
(1, 2, '2012-12-18'),
(2, 1, '2014-04-23'),
(3, 2, '2001-06-21'),
(3, 3, '2007-11-15');

-- --------------------------------------------------------

--
-- Structure de la table `r_require`
--

CREATE TABLE `r_require` (
  `KoLIC_CODE` int(11) NOT NULL,
  `QUALIF_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_require`:
--   `KoLIC_CODE`
--       `kolicense` -> `KoLIC_CODE`
--   `QUALIF_CODE`
--       `qualifications` -> `QUALIF_CODE`
--

--
-- Déchargement des données de la table `r_require`
--

INSERT INTO `r_require` (`KoLIC_CODE`, `QUALIF_CODE`) VALUES
(1, 1),
(1, 3),
(3, 2),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `r_serve`
--

CREATE TABLE `r_serve` (
  `AIRPORT_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CITY_CODE` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `r_serve`:
--   `AIRPORT_CODE`
--       `airports` -> `AIRPORT_CODE`
--   `CITY_CODE`
--       `cities` -> `CITY_CODE`
--

--
-- Déchargement des données de la table `r_serve`
--

INSERT INTO `r_serve` (`AIRPORT_CODE`, `CITY_CODE`) VALUES
('EJHD', 'SF'),
('PACH', 'PARIS');

-- --------------------------------------------------------

--
-- Structure de la table `staff`
--

CREATE TABLE `staff` (
  `STAFF_CODE` int(11) NOT NULL,
  `LNAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FNAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BIRTH` date DEFAULT NULL,
  `MEMBER_POSITION` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FLYING` smallint(6) DEFAULT NULL,
  `TFH` smallint(6) DEFAULT NULL,
  `STATUS_CODE` smallint(6) NOT NULL,
  `MANAGER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `staff`:
--   `STATUS_CODE`
--       `status` -> `STATUS_CODE`
--   `MANAGER`
--       `staff` -> `STAFF_CODE`
--

--
-- Déchargement des données de la table `staff`
--

INSERT INTO `staff` (`STAFF_CODE`, `LNAME`, `FNAME`, `BIRTH`, `MEMBER_POSITION`, `FLYING`, `TFH`, `STATUS_CODE`, `MANAGER`) VALUES
(1, 'DUBOIS', 'Patrick', '1980-04-18', 'MANAGER', 0, NULL, 2, NULL),
(2, 'DUSQUENOY', 'Cedric', '1991-10-29', 'PILOT', 1, 3421, 3, 1),
(3, 'GIROUX', 'Christien', '1989-12-07', 'FO', 1, 1486, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

CREATE TABLE `status` (
  `STATUS_CODE` smallint(6) NOT NULL,
  `STATUS_NAME` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `STATUS_DSCR` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `status`:
--

--
-- Déchargement des données de la table `status`
--

INSERT INTO `status` (`STATUS_CODE`, `STATUS_NAME`, `STATUS_DSCR`) VALUES
(1, 'HS', 'Hors Service'),
(2, 'OK', 'Fonctionne correction'),
(3, 'UNKNOW', 'Status Inconnue');

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `SEAT` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PRICE` decimal(19,4) DEFAULT NULL,
  `PSGR_CODE` int(11) NOT NULL,
  `CURR_CODE` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CLASS_CODE` int(11) NOT NULL,
  `EFF_FLIGHTS_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `tickets`:
--   `PSGR_CODE`
--       `passengers` -> `PSGR_CODE`
--   `CURR_CODE`
--       `currencies` -> `CURR_CODE`
--   `CLASS_CODE`
--       `classes` -> `CLASS_CODE`
--   `EFF_FLIGHTS_FK`
--       `eff_flights` -> `EFF_FLIGHTS_PK`
--

--
-- Déchargement des données de la table `tickets`
--

INSERT INTO `tickets` (`SEAT`, `PRICE`, `PSGR_CODE`, `CURR_CODE`, `CLASS_CODE`, `EFF_FLIGHTS_FK`) VALUES
('54', '2300.0000', 1, 'EUR', 1, 1),
('87', '1010.0000', 2, 'USD', 3, 2),
('A98', '1999.9900', 1, 'YEN', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `timeslots`
--

CREATE TABLE `timeslots` (
  `TIMESLOT_CODE` int(11) NOT NULL,
  `BEGINNING` time NOT NULL,
  `ENDING` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `timeslots`:
--

--
-- Déchargement des données de la table `timeslots`
--

INSERT INTO `timeslots` (`TIMESLOT_CODE`, `BEGINNING`, `ENDING`) VALUES
(1, '00:00:00', '23:59:59'),
(2, '12:00:00', '02:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `weekdays`
--

CREATE TABLE `weekdays` (
  `DAY_NBR` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `weekdays`:
--

--
-- Déchargement des données de la table `weekdays`
--

INSERT INTO `weekdays` (`DAY_NBR`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7);

-- --------------------------------------------------------

--
-- Structure de la table `year_periods`
--

CREATE TABLE `year_periods` (
  `YEAR_PERIOD` char(4) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELATIONS POUR LA TABLE `year_periods`:
--

--
-- Déchargement des données de la table `year_periods`
--

INSERT INTO `year_periods` (`YEAR_PERIOD`) VALUES
('2019'),
('2020'),
('2021'),
('2022');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `aircrafts`
--
ALTER TABLE `aircrafts`
  ADD PRIMARY KEY (`AIRCRAFT_NBR`),
  ADD KEY `IATA` (`IATA`);

--
-- Index pour la table `airlinks`
--
ALTER TABLE `airlinks`
  ADD PRIMARY KEY (`AIRLINK_CODE`),
  ADD KEY `AIRPORT_CODE_FROM` (`AIRPORT_CODE_FROM`),
  ADD KEY `AIRPORT_CODE_TO` (`AIRPORT_CODE_TO`);

--
-- Index pour la table `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`AIRPORT_CODE`),
  ADD KEY `CURR_CODE` (`CURR_CODE`);

--
-- Index pour la table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`ASSIGNT_NBR`),
  ADD KEY `STAFF_CODE` (`STAFF_CODE`),
  ADD KEY `ROLE_CODE` (`ROLE_CODE`),
  ADD KEY `CREW_CODE` (`CREW_CODE`);

--
-- Index pour la table `checkups`
--
ALTER TABLE `checkups`
  ADD PRIMARY KEY (`CHECKUP_NBR`),
  ADD UNIQUE KEY `STAFF_CODE` (`STAFF_CODE`);

--
-- Index pour la table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`CITY_CODE`);

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`CLASS_CODE`);

--
-- Index pour la table `crews`
--
ALTER TABLE `crews`
  ADD PRIMARY KEY (`CREW_CODE`);

--
-- Index pour la table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`CURR_CODE`);

--
-- Index pour la table `day_periods`
--
ALTER TABLE `day_periods`
  ADD PRIMARY KEY (`DAY_PERIOD`),
  ADD KEY `MONTH_PERIOD` (`MONTH_PERIOD`),
  ADD KEY `DAY_NBR` (`DAY_NBR`);

--
-- Index pour la table `doc`
--
ALTER TABLE `doc`
  ADD PRIMARY KEY (`DOC_CODE`);

--
-- Index pour la table `eff_flights`
--
ALTER TABLE `eff_flights`
  ADD PRIMARY KEY (`EFF_FLIGHTS_PK`),
  ADD KEY `CREW_CODE` (`CREW_CODE`),
  ADD KEY `AIRCRAFT_NBR` (`AIRCRAFT_NBR`),
  ADD KEY `PLAN_DAYFLIGHTS_FK` (`PLAN_DAYFLIGHTS_FK`);

--
-- Index pour la table `eff_stopovers`
--
ALTER TABLE `eff_stopovers`
  ADD PRIMARY KEY (`ESO_NBR`),
  ADD KEY `STOPOVER_NBR` (`STOPOVER_NBR`),
  ADD KEY `EFF_FLIGHTS_FK` (`EFF_FLIGHTS_FK`);

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`EVENT_NBR`),
  ADD UNIQUE KEY `STAFF_CODE` (`STAFF_CODE`),
  ADD KEY `KoEVT_CODE` (`KoEVT_CODE`),
  ADD KEY `STATUS_CODE` (`STATUS_CODE`);

--
-- Index pour la table `koaircraft`
--
ALTER TABLE `koaircraft`
  ADD PRIMARY KEY (`IATA`);

--
-- Index pour la table `kocrew`
--
ALTER TABLE `kocrew`
  ADD PRIMARY KEY (`KoCREW_CODE`);

--
-- Index pour la table `koevent`
--
ALTER TABLE `koevent`
  ADD PRIMARY KEY (`KoEVT_CODE`);

--
-- Index pour la table `kolicense`
--
ALTER TABLE `kolicense`
  ADD PRIMARY KEY (`KoLIC_CODE`);

--
-- Index pour la table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`LNG_CODE`);

--
-- Index pour la table `month_periods`
--
ALTER TABLE `month_periods`
  ADD PRIMARY KEY (`MONTH_PERIOD`,`YEAR_PERIOD`) USING BTREE,
  ADD KEY `YEAR_PERIOD` (`YEAR_PERIOD`);

--
-- Index pour la table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`PSGR_CODE`);

--
-- Index pour la table `plan_dayflights`
--
ALTER TABLE `plan_dayflights`
  ADD PRIMARY KEY (`PLAN_DAYFLIGHTS_PK`),
  ADD KEY `IATA` (`IATA`),
  ADD KEY `FLIGHT_NBR` (`FLIGHT_NBR`),
  ADD KEY `DAY_NBR` (`DAY_NBR`);

--
-- Index pour la table `plan_flights`
--
ALTER TABLE `plan_flights`
  ADD PRIMARY KEY (`FLIGHT_NBR`),
  ADD KEY `AIRLINK_CODE` (`AIRLINK_CODE`);

--
-- Index pour la table `plan_stopovers`
--
ALTER TABLE `plan_stopovers`
  ADD PRIMARY KEY (`STOPOVER_NBR`),
  ADD KEY `ROUTE_CODE` (`ROUTE_CODE`),
  ADD KEY `PLAN_DAYFLIGHTS_FK` (`PLAN_DAYFLIGHTS_FK`);

--
-- Index pour la table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`QUALIF_CODE`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ROLE_CODE`);

--
-- Index pour la table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`ROUTE_CODE`),
  ADD KEY `AIRLINK_CODE` (`AIRLINK_CODE`),
  ADD KEY `IATA` (`IATA`);

--
-- Index pour la table `r_accept`
--
ALTER TABLE `r_accept`
  ADD PRIMARY KEY (`AIRPORT_CODE`,`IATA`),
  ADD KEY `IATA` (`IATA`);

--
-- Index pour la table `r_allow_pilot`
--
ALTER TABLE `r_allow_pilot`
  ADD PRIMARY KEY (`KoLIC_CODE`,`IATA`),
  ADD KEY `IATA` (`IATA`);

--
-- Index pour la table `r_bind`
--
ALTER TABLE `r_bind`
  ADD PRIMARY KEY (`EVENT_NBR`,`DOC_CODE`),
  ADD KEY `DOC_CODE` (`DOC_CODE`);

--
-- Index pour la table `r_contain`
--
ALTER TABLE `r_contain`
  ADD PRIMARY KEY (`CLASS_CODE`,`AIRCRAFT_NBR`),
  ADD KEY `AIRCRAFT_NBR` (`AIRCRAFT_NBR`);

--
-- Index pour la table `r_estimate_nb_passengers`
--
ALTER TABLE `r_estimate_nb_passengers`
  ADD PRIMARY KEY (`TIMESLOT_CODE`,`AIRLINK_CODE`,`DAY_NBR`),
  ADD KEY `AIRLINK_CODE` (`AIRLINK_CODE`),
  ADD KEY `DAY_NBR` (`DAY_NBR`);

--
-- Index pour la table `r_estimate_price`
--
ALTER TABLE `r_estimate_price`
  ADD PRIMARY KEY (`CLASS_CODE`,`CURR_CODE`,`PLAN_DAYFLIGHTS_FK`),
  ADD KEY `CURR_CODE` (`CURR_CODE`),
  ADD KEY `PLAN_DAYFLIGHTS_FK` (`PLAN_DAYFLIGHTS_FK`);

--
-- Index pour la table `r_have_qualif`
--
ALTER TABLE `r_have_qualif`
  ADD PRIMARY KEY (`QUALIF_CODE`,`STAFF_CODE`),
  ADD KEY `STAFF_CODE` (`STAFF_CODE`);

--
-- Index pour la table `r_include`
--
ALTER TABLE `r_include`
  ADD PRIMARY KEY (`KoCREW_CODE`,`ROLE_CODE`),
  ADD KEY `ROLE_CODE` (`ROLE_CODE`);

--
-- Index pour la table `r_in_flight`
--
ALTER TABLE `r_in_flight`
  ADD PRIMARY KEY (`STAFF_CODE`,`MONTH_PERIOD`,`YEAR_PERIOD`) USING BTREE,
  ADD KEY `MONTH_PERIOD` (`MONTH_PERIOD`),
  ADD KEY `YEAR_PERIOD` (`YEAR_PERIOD`);

--
-- Index pour la table `r_isa_kocrew`
--
ALTER TABLE `r_isa_kocrew`
  ADD PRIMARY KEY (`CREW_CODE`,`KoCREW_CODE`),
  ADD KEY `KoCREW_CODE` (`KoCREW_CODE`);

--
-- Index pour la table `r_may_have`
--
ALTER TABLE `r_may_have`
  ADD PRIMARY KEY (`STAFF_CODE`,`ROLE_CODE`),
  ADD KEY `ROLE_CODE` (`ROLE_CODE`);

--
-- Index pour la table `r_name_airport`
--
ALTER TABLE `r_name_airport`
  ADD PRIMARY KEY (`AIRPORT_CODE`,`LNG_CODE`),
  ADD KEY `LNG_CODE` (`LNG_CODE`);

--
-- Index pour la table `r_name_city`
--
ALTER TABLE `r_name_city`
  ADD PRIMARY KEY (`CITY_CODE`,`LNG_CODE`),
  ADD KEY `LNG_CODE` (`LNG_CODE`);

--
-- Index pour la table `r_name_day`
--
ALTER TABLE `r_name_day`
  ADD PRIMARY KEY (`DAY_NBR`,`LNG_CODE`),
  ADD KEY `LNG_CODE` (`LNG_CODE`);

--
-- Index pour la table `r_name_koaircraft`
--
ALTER TABLE `r_name_koaircraft`
  ADD PRIMARY KEY (`LNG_CODE`,`IATA`),
  ADD KEY `IATA` (`IATA`);

--
-- Index pour la table `r_name_language`
--
ALTER TABLE `r_name_language`
  ADD PRIMARY KEY (`LNG_CODE_IN`,`LNG_CODE_OF`),
  ADD KEY `LNG_CODE_OF` (`LNG_CODE_OF`);

--
-- Index pour la table `r_name_month`
--
ALTER TABLE `r_name_month`
  ADD PRIMARY KEY (`MONTH_PERIOD`,`LNG_CODE`),
  ADD KEY `LNG_CODE` (`LNG_CODE`);

--
-- Index pour la table `r_name_role`
--
ALTER TABLE `r_name_role`
  ADD PRIMARY KEY (`ROLE_CODE`,`LNG_CODE`),
  ADD KEY `LNG_CODE` (`LNG_CODE`);

--
-- Index pour la table `r_need`
--
ALTER TABLE `r_need`
  ADD PRIMARY KEY (`IATA`,`KoCREW_CODE`),
  ADD KEY `KoCREW_CODE` (`KoCREW_CODE`);

--
-- Index pour la table `r_obtain`
--
ALTER TABLE `r_obtain`
  ADD PRIMARY KEY (`KoLIC_CODE`,`STAFF_CODE`),
  ADD KEY `STAFF_CODE` (`STAFF_CODE`);

--
-- Index pour la table `r_require`
--
ALTER TABLE `r_require`
  ADD PRIMARY KEY (`KoLIC_CODE`,`QUALIF_CODE`),
  ADD KEY `QUALIF_CODE` (`QUALIF_CODE`);

--
-- Index pour la table `r_serve`
--
ALTER TABLE `r_serve`
  ADD PRIMARY KEY (`AIRPORT_CODE`,`CITY_CODE`),
  ADD KEY `CITY_CODE` (`CITY_CODE`);

--
-- Index pour la table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`STAFF_CODE`),
  ADD KEY `STATUS_CODE` (`STATUS_CODE`),
  ADD KEY `MANAGER` (`MANAGER`);

--
-- Index pour la table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`STATUS_CODE`);

--
-- Index pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`SEAT`),
  ADD KEY `PSGR_CODE` (`PSGR_CODE`),
  ADD KEY `CURR_CODE` (`CURR_CODE`),
  ADD KEY `CLASS_CODE` (`CLASS_CODE`),
  ADD KEY `EFF_FLIGHTS_FK` (`EFF_FLIGHTS_FK`);

--
-- Index pour la table `timeslots`
--
ALTER TABLE `timeslots`
  ADD PRIMARY KEY (`TIMESLOT_CODE`);

--
-- Index pour la table `weekdays`
--
ALTER TABLE `weekdays`
  ADD PRIMARY KEY (`DAY_NBR`);

--
-- Index pour la table `year_periods`
--
ALTER TABLE `year_periods`
  ADD PRIMARY KEY (`YEAR_PERIOD`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `aircrafts`
--
ALTER TABLE `aircrafts`
  MODIFY `AIRCRAFT_NBR` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `airlinks`
--
ALTER TABLE `airlinks`
  MODIFY `AIRLINK_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `ASSIGNT_NBR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `checkups`
--
ALTER TABLE `checkups`
  MODIFY `CHECKUP_NBR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `classes`
--
ALTER TABLE `classes`
  MODIFY `CLASS_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `crews`
--
ALTER TABLE `crews`
  MODIFY `CREW_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `doc`
--
ALTER TABLE `doc`
  MODIFY `DOC_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `eff_flights`
--
ALTER TABLE `eff_flights`
  MODIFY `EFF_FLIGHTS_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `eff_stopovers`
--
ALTER TABLE `eff_stopovers`
  MODIFY `ESO_NBR` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4587;

--
-- AUTO_INCREMENT pour la table `events`
--
ALTER TABLE `events`
  MODIFY `EVENT_NBR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `kocrew`
--
ALTER TABLE `kocrew`
  MODIFY `KoCREW_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `koevent`
--
ALTER TABLE `koevent`
  MODIFY `KoEVT_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `kolicense`
--
ALTER TABLE `kolicense`
  MODIFY `KoLIC_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `PSGR_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `plan_dayflights`
--
ALTER TABLE `plan_dayflights`
  MODIFY `PLAN_DAYFLIGHTS_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `plan_flights`
--
ALTER TABLE `plan_flights`
  MODIFY `FLIGHT_NBR` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `plan_stopovers`
--
ALTER TABLE `plan_stopovers`
  MODIFY `STOPOVER_NBR` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `QUALIF_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `ROLE_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `routes`
--
ALTER TABLE `routes`
  MODIFY `ROUTE_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `staff`
--
ALTER TABLE `staff`
  MODIFY `STAFF_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `status`
--
ALTER TABLE `status`
  MODIFY `STATUS_CODE` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `timeslots`
--
ALTER TABLE `timeslots`
  MODIFY `TIMESLOT_CODE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `weekdays`
--
ALTER TABLE `weekdays`
  MODIFY `DAY_NBR` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `aircrafts`
--
ALTER TABLE `aircrafts`
  ADD CONSTRAINT `aircrafts_ibfk_1` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `airlinks`
--
ALTER TABLE `airlinks`
  ADD CONSTRAINT `airlinks_ibfk_1` FOREIGN KEY (`AIRPORT_CODE_FROM`) REFERENCES `airports` (`AIRPORT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `airlinks_ibfk_2` FOREIGN KEY (`AIRPORT_CODE_TO`) REFERENCES `airports` (`AIRPORT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `airports`
--
ALTER TABLE `airports`
  ADD CONSTRAINT `airports_ibfk_1` FOREIGN KEY (`CURR_CODE`) REFERENCES `currencies` (`CURR_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_ibfk_2` FOREIGN KEY (`ROLE_CODE`) REFERENCES `roles` (`ROLE_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_ibfk_3` FOREIGN KEY (`CREW_CODE`) REFERENCES `crews` (`CREW_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `checkups`
--
ALTER TABLE `checkups`
  ADD CONSTRAINT `checkups_ibfk_1` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `day_periods`
--
ALTER TABLE `day_periods`
  ADD CONSTRAINT `day_periods_ibfk_1` FOREIGN KEY (`MONTH_PERIOD`) REFERENCES `month_periods` (`MONTH_PERIOD`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `day_periods_ibfk_2` FOREIGN KEY (`DAY_NBR`) REFERENCES `weekdays` (`DAY_NBR`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `eff_flights`
--
ALTER TABLE `eff_flights`
  ADD CONSTRAINT `eff_flights_ibfk_1` FOREIGN KEY (`CREW_CODE`) REFERENCES `crews` (`CREW_CODE`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `eff_flights_ibfk_2` FOREIGN KEY (`AIRCRAFT_NBR`) REFERENCES `aircrafts` (`AIRCRAFT_NBR`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `eff_flights_ibfk_3` FOREIGN KEY (`PLAN_DAYFLIGHTS_FK`) REFERENCES `plan_dayflights` (`PLAN_DAYFLIGHTS_PK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `eff_stopovers`
--
ALTER TABLE `eff_stopovers`
  ADD CONSTRAINT `eff_stopovers_ibfk_1` FOREIGN KEY (`STOPOVER_NBR`) REFERENCES `plan_stopovers` (`STOPOVER_NBR`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eff_stopovers_ibfk_2` FOREIGN KEY (`EFF_FLIGHTS_FK`) REFERENCES `eff_flights` (`EFF_FLIGHTS_PK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`KoEVT_CODE`) REFERENCES `koevent` (`KoEVT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `events_ibfk_3` FOREIGN KEY (`STATUS_CODE`) REFERENCES `status` (`STATUS_CODE`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `month_periods`
--
ALTER TABLE `month_periods`
  ADD CONSTRAINT `month_periods_ibfk_1` FOREIGN KEY (`YEAR_PERIOD`) REFERENCES `year_periods` (`YEAR_PERIOD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `plan_dayflights`
--
ALTER TABLE `plan_dayflights`
  ADD CONSTRAINT `plan_dayflights_ibfk_1` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `plan_dayflights_ibfk_2` FOREIGN KEY (`FLIGHT_NBR`) REFERENCES `plan_flights` (`FLIGHT_NBR`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plan_dayflights_ibfk_3` FOREIGN KEY (`DAY_NBR`) REFERENCES `weekdays` (`DAY_NBR`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `plan_flights`
--
ALTER TABLE `plan_flights`
  ADD CONSTRAINT `plan_flights_ibfk_1` FOREIGN KEY (`AIRLINK_CODE`) REFERENCES `airlinks` (`AIRLINK_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `plan_stopovers`
--
ALTER TABLE `plan_stopovers`
  ADD CONSTRAINT `plan_stopovers_ibfk_1` FOREIGN KEY (`ROUTE_CODE`) REFERENCES `routes` (`ROUTE_CODE`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `plan_stopovers_ibfk_2` FOREIGN KEY (`PLAN_DAYFLIGHTS_FK`) REFERENCES `plan_dayflights` (`PLAN_DAYFLIGHTS_PK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `routes`
--
ALTER TABLE `routes`
  ADD CONSTRAINT `routes_ibfk_1` FOREIGN KEY (`AIRLINK_CODE`) REFERENCES `airlinks` (`AIRLINK_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `routes_ibfk_2` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_accept`
--
ALTER TABLE `r_accept`
  ADD CONSTRAINT `r_accept_ibfk_1` FOREIGN KEY (`AIRPORT_CODE`) REFERENCES `airports` (`AIRPORT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_accept_ibfk_2` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_allow_pilot`
--
ALTER TABLE `r_allow_pilot`
  ADD CONSTRAINT `r_allow_pilot_ibfk_1` FOREIGN KEY (`KoLIC_CODE`) REFERENCES `kolicense` (`KoLIC_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_allow_pilot_ibfk_2` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_bind`
--
ALTER TABLE `r_bind`
  ADD CONSTRAINT `r_bind_ibfk_1` FOREIGN KEY (`EVENT_NBR`) REFERENCES `events` (`EVENT_NBR`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_bind_ibfk_2` FOREIGN KEY (`DOC_CODE`) REFERENCES `doc` (`DOC_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_contain`
--
ALTER TABLE `r_contain`
  ADD CONSTRAINT `r_contain_ibfk_1` FOREIGN KEY (`CLASS_CODE`) REFERENCES `classes` (`CLASS_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_contain_ibfk_2` FOREIGN KEY (`AIRCRAFT_NBR`) REFERENCES `aircrafts` (`AIRCRAFT_NBR`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_estimate_nb_passengers`
--
ALTER TABLE `r_estimate_nb_passengers`
  ADD CONSTRAINT `r_estimate_nb_passengers_ibfk_1` FOREIGN KEY (`TIMESLOT_CODE`) REFERENCES `timeslots` (`TIMESLOT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_estimate_nb_passengers_ibfk_2` FOREIGN KEY (`AIRLINK_CODE`) REFERENCES `airlinks` (`AIRLINK_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_estimate_nb_passengers_ibfk_3` FOREIGN KEY (`DAY_NBR`) REFERENCES `weekdays` (`DAY_NBR`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_estimate_price`
--
ALTER TABLE `r_estimate_price`
  ADD CONSTRAINT `r_estimate_price_ibfk_1` FOREIGN KEY (`CLASS_CODE`) REFERENCES `classes` (`CLASS_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_estimate_price_ibfk_2` FOREIGN KEY (`CURR_CODE`) REFERENCES `currencies` (`CURR_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_estimate_price_ibfk_3` FOREIGN KEY (`PLAN_DAYFLIGHTS_FK`) REFERENCES `plan_dayflights` (`PLAN_DAYFLIGHTS_PK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_have_qualif`
--
ALTER TABLE `r_have_qualif`
  ADD CONSTRAINT `r_have_qualif_ibfk_1` FOREIGN KEY (`QUALIF_CODE`) REFERENCES `qualifications` (`QUALIF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_have_qualif_ibfk_2` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_include`
--
ALTER TABLE `r_include`
  ADD CONSTRAINT `r_include_ibfk_1` FOREIGN KEY (`KoCREW_CODE`) REFERENCES `kocrew` (`KoCREW_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_include_ibfk_2` FOREIGN KEY (`ROLE_CODE`) REFERENCES `roles` (`ROLE_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_in_flight`
--
ALTER TABLE `r_in_flight`
  ADD CONSTRAINT `r_in_flight_ibfk_1` FOREIGN KEY (`MONTH_PERIOD`) REFERENCES `month_periods` (`MONTH_PERIOD`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_in_flight_ibfk_2` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_in_flight_ibfk_3` FOREIGN KEY (`YEAR_PERIOD`) REFERENCES `month_periods` (`YEAR_PERIOD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_isa_kocrew`
--
ALTER TABLE `r_isa_kocrew`
  ADD CONSTRAINT `r_isa_kocrew_ibfk_1` FOREIGN KEY (`CREW_CODE`) REFERENCES `crews` (`CREW_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_isa_kocrew_ibfk_2` FOREIGN KEY (`KoCREW_CODE`) REFERENCES `kocrew` (`KoCREW_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_may_have`
--
ALTER TABLE `r_may_have`
  ADD CONSTRAINT `r_may_have_ibfk_1` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_may_have_ibfk_2` FOREIGN KEY (`ROLE_CODE`) REFERENCES `roles` (`ROLE_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_airport`
--
ALTER TABLE `r_name_airport`
  ADD CONSTRAINT `r_name_airport_ibfk_1` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_airport_ibfk_2` FOREIGN KEY (`AIRPORT_CODE`) REFERENCES `airports` (`AIRPORT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_city`
--
ALTER TABLE `r_name_city`
  ADD CONSTRAINT `r_name_city_ibfk_1` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_city_ibfk_2` FOREIGN KEY (`CITY_CODE`) REFERENCES `cities` (`CITY_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_day`
--
ALTER TABLE `r_name_day`
  ADD CONSTRAINT `r_name_day_ibfk_1` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_day_ibfk_2` FOREIGN KEY (`DAY_NBR`) REFERENCES `weekdays` (`DAY_NBR`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_koaircraft`
--
ALTER TABLE `r_name_koaircraft`
  ADD CONSTRAINT `r_name_koaircraft_ibfk_1` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_koaircraft_ibfk_2` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_language`
--
ALTER TABLE `r_name_language`
  ADD CONSTRAINT `r_name_language_ibfk_1` FOREIGN KEY (`LNG_CODE_IN`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_language_ibfk_2` FOREIGN KEY (`LNG_CODE_OF`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_month`
--
ALTER TABLE `r_name_month`
  ADD CONSTRAINT `r_name_month_ibfk_1` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_month_ibfk_2` FOREIGN KEY (`MONTH_PERIOD`) REFERENCES `month_periods` (`MONTH_PERIOD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_name_role`
--
ALTER TABLE `r_name_role`
  ADD CONSTRAINT `r_name_role_ibfk_1` FOREIGN KEY (`ROLE_CODE`) REFERENCES `roles` (`ROLE_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_name_role_ibfk_2` FOREIGN KEY (`LNG_CODE`) REFERENCES `languages` (`LNG_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_need`
--
ALTER TABLE `r_need`
  ADD CONSTRAINT `r_need_ibfk_1` FOREIGN KEY (`IATA`) REFERENCES `koaircraft` (`IATA`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_need_ibfk_2` FOREIGN KEY (`KoCREW_CODE`) REFERENCES `kocrew` (`KoCREW_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_obtain`
--
ALTER TABLE `r_obtain`
  ADD CONSTRAINT `r_obtain_ibfk_1` FOREIGN KEY (`KoLIC_CODE`) REFERENCES `kolicense` (`KoLIC_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_obtain_ibfk_2` FOREIGN KEY (`STAFF_CODE`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_require`
--
ALTER TABLE `r_require`
  ADD CONSTRAINT `r_require_ibfk_1` FOREIGN KEY (`KoLIC_CODE`) REFERENCES `kolicense` (`KoLIC_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_require_ibfk_2` FOREIGN KEY (`QUALIF_CODE`) REFERENCES `qualifications` (`QUALIF_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `r_serve`
--
ALTER TABLE `r_serve`
  ADD CONSTRAINT `r_serve_ibfk_1` FOREIGN KEY (`AIRPORT_CODE`) REFERENCES `airports` (`AIRPORT_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_serve_ibfk_2` FOREIGN KEY (`CITY_CODE`) REFERENCES `cities` (`CITY_CODE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`STATUS_CODE`) REFERENCES `status` (`STATUS_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `staff_ibfk_2` FOREIGN KEY (`MANAGER`) REFERENCES `staff` (`STAFF_CODE`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`PSGR_CODE`) REFERENCES `passengers` (`PSGR_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`CURR_CODE`) REFERENCES `currencies` (`CURR_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`CLASS_CODE`) REFERENCES `classes` (`CLASS_CODE`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`EFF_FLIGHTS_FK`) REFERENCES `eff_flights` (`EFF_FLIGHTS_PK`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
