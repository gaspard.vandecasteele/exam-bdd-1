-- Gaspard VANDECASTEELE
-- Comment those lines if you don't want to create database
-- automaticaly

DROP DATABASE IF EXISTS db_flight;
CREATE DATABASE db_flight
	CHARACTER SET = 'utf8mb4'
	COLLATE = 'utf8mb4_unicode_ci';
	
USE db_flight;

-- TABLES
-- Airports Table
CREATE TABLE IF NOT EXISTS AIRPORTS (
	AIRPORT_CODE CHAR(4) PRIMARY KEY
);

-- Cities Table
CREATE TABLE IF NOT EXISTS CITIES (
	CITY_CODE VARCHAR(25) PRIMARY KEY,
	CITY_GPS VARCHAR(50)
);

-- Languages Table
CREATE TABLE IF NOT EXISTS LANGUAGES (
	LNG_CODE CHAR(2) PRIMARY KEY
);

-- Timeslot Table
CREATE TABLE IF NOT EXISTS TIMESLOTS (
	TIMESLOT_CODE INT AUTO_INCREMENT PRIMARY KEY,
	BEGINNING TIME NOT NULL,
	ENDING TIME NOT NULL
);

-- Aircraft Types Table
CREATE TABLE IF NOT EXISTS KoAIRCRAFT (
	IATA CHAR(4) PRIMARY KEY,
	AUTONOMY INT,
	SPEED INT,
	SEATS INT,
	MAX_LOAD INT,
	Max_FUEL INT
);

-- Aircrafts Table
CREATE TABLE IF NOT EXISTS AIRCRAFTS (
	AIRCRAFT_NBR SMALLINT AUTO_INCREMENT PRIMARY KEY,
	LAST_SERVICING DATE,
	TOTALFLIGHTHOURS INT
);

-- Crew Types Table
CREATE TABLE IF NOT EXISTS KoCREW (
	KoCREW_CODE INT AUTO_INCREMENT PRIMARY KEY,
	KoCREW_NAME VARCHAR(50)
);

-- Crews Table
CREATE TABLE IF NOT EXISTS CREWS (
	CREW_CODE INT AUTO_INCREMENT PRIMARY KEY,
	CREW_NAME VARCHAR(50)
);

-- Weekdays TABLE
CREATE TABLE IF NOT EXISTS WEEKDAYS (
	DAY_NBR SMALLINT AUTO_INCREMENT PRIMARY KEY
);

-- Routes Tables
CREATE TABLE IF NOT EXISTS ROUTES (
	ROUTE_CODE INT AUTO_INCREMENT PRIMARY KEY,
	ROUTE_DURATION TIME
);

-- Airlinks Table
CREATE TABLE IF NOT EXISTS AIRLINKS (
	AIRLINK_CODE INT AUTO_INCREMENT PRIMARY KEY,
	AIRLINK_DISTANCE VARCHAR(50)
);

-- Planned Flights Table
CREATE TABLE IF NOT EXISTS PLAN_FLIGHTS (
	FLIGHT_NBR SMALLINT AUTO_INCREMENT PRIMARY KEY,
	DEPARTURE TIME,
	FREQUENCY SMALLINT
);

-- Planned Day Flights Table
CREATE TABLE IF NOT EXISTS PLAN_DAYFLIGHTS (
	PLAN_DAYFLIGHTS_PK INT AUTO_INCREMENT PRIMARY KEY,
	PLAN_SEATS SMALLINT,
	PLAN_DURATION TIME,
	PLAN_ARR_TIME TIME
);

-- Effective Flights Table
CREATE TABLE IF NOT EXISTS EFF_FLIGHTS (
	EFF_FLIGHTS_PK INT AUTO_INCREMENT PRIMARY KEY,
	PLAN_DEP_DATE TIME,
	RES_SEATS_QTY SMALLINT,
	OCC_SEATS_QTY SMALLINT,
	REAL_DEP_TIME TIMESTAMP,
	REAL_ARR_TIME TIMESTAMP,
	LOADED_FUEL INT
);

-- Planned Stopovers Table
CREATE TABLE IF NOT EXISTS PLAN_STOPOVERS (
	STOPOVER_NBR SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Duration TIME
);

-- Effective Stopovers Table
CREATE TABLE IF NOT EXISTS EFF_STOPOVERS (
	ESO_NBR SMALLINT AUTO_INCREMENT PRIMARY KEY,
	REAL_TRAVEL_DUR TIME,
	REAL_LANDING_TIME TIMESTAMP,
	REAL_TAKEOFF_TIME TIMESTAMP,
	LOADED_FUEL INT
);

-- Classes Table
CREATE TABLE IF NOT EXISTS CLASSES (
	CLASS_CODE INT AUTO_INCREMENT PRIMARY KEY,
	CLASS_NAME VARCHAR(50)
);

-- Currencies Table
CREATE TABLE IF NOT EXISTS CURRENCIES (
	CURR_CODE CHAR(4) PRIMARY KEY,
	CURR_NAME VARCHAR(50)
);

-- Assignments Table
CREATE TABLE IF NOT EXISTS ASSIGNMENTS (
	ASSIGNT_NBR INT AUTO_INCREMENT PRIMARY KEY,
	STARTING_DATE DATE,
	ENDING_DATE DATE
);

-- Roles Table
CREATE TABLE IF NOT EXISTS ROLES (
	ROLE_CODE INT AUTO_INCREMENT PRIMARY KEY
);

-- Tickets Table
CREATE TABLE IF NOT EXISTS TICKETS (
	SEAT CHAR(5) PRIMARY KEY,
	PRICE DECIMAL(19,4)
);

-- Day Periods Table
CREATE TABLE IF NOT EXISTS DAY_PERIODS (
	DAY_PERIOD CHAR(2)
);

-- Month Periods Table
CREATE TABLE IF NOT EXISTS MONTH_PERIODS (
	MONTH_PERIOD CHAR(2)
);

-- Passengers Table
CREATE TABLE IF NOT EXISTS PASSENGERS (
	PSGR_CODE INT AUTO_INCREMENT PRIMARY KEY,
	LNAME TIME,
	FNAME VARCHAR(50)
);

-- Staff table
CREATE TABLE IF NOT EXISTS STAFF (
	STAFF_CODE INT AUTO_INCREMENT PRIMARY KEY,
	LNAME VARCHAR(50),
	FNAME VARCHAR(50),
	BIRTH DATE,
	MEMBER_POSITION VARCHAR(50),
	FLYING SMALLINT,
	TFH SMALLINT
);

-- Qualifications Table
CREATE TABLE IF NOT EXISTS QUALIFICATIONS (
	QUALIF_CODE INT AUTO_INCREMENT PRIMARY KEY,
	QUALIF_NAME VARCHAR(50),
	QUALIF_DSCR TEXT
);

-- Year Periods Table
CREATE TABLE IF NOT EXISTS YEAR_PERIODS (
	YEAR_PERIOD CHAR(4) PRIMARY KEY
);

-- LicenseTypes Table
CREATE TABLE IF NOT EXISTS KoLICENSE (
	KoLIC_CODE INT AUTO_INCREMENT PRIMARY KEY,
	KoLIC_NAME VARCHAR(50)
);

-- Documents Table
CREATE TABLE IF NOT EXISTS DOC (
	DOC_CODE INT AUTO_INCREMENT PRIMARY KEY,
	DOC_NAME VARCHAR(50),
	DOC_DATE DATE,
	DOC_LINK VARCHAR(50)
);

-- Medical_Checkup Table
CREATE TABLE IF NOT EXISTS CHECKUPS (
	CHECKUP_NBR INT AUTO_INCREMENT PRIMARY KEY,
	CHECKUP_DATE DATE
);

-- Status Table
CREATE TABLE IF NOT EXISTS STATUS (
	STATUS_CODE SMALLINT AUTO_INCREMENT PRIMARY KEY,
	STATUS_NAME VARCHAR(50),
	STATUS_DSCR TEXT
);
-- Events Table
CREATE TABLE IF NOT EXISTS EVENTS (
	EVENT_NBR INT AUTO_INCREMENT PRIMARY KEY,
	EVENT_LABEL VARCHAR(50),
	EVENT_DSCR TEXT,
	EVENT_DATE DATE,
	EVENT_TIME TIME
);

-- Event Types
CREATE TABLE IF NOT EXISTS KoEVENT (
	KoEVT_CODE INT AUTO_INCREMENT PRIMARY KEY,
	KoEVT_NAME VARCHAR(50)
);

-- REFERENCES
-- Reference Aircraft -> Aircraft Types (is_a_AT)
ALTER TABLE AIRCRAFTS
	ADD COLUMN (IATA CHAR(4) NOT NULL),
	ADD FOREIGN KEY(IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference EFF_FLIGHTS -> CREWS (Assign_Crew)
ALTER TABLE EFF_FLIGHTS
	ADD COLUMN (CREW_CODE INT),
	ADD FOREIGN KEY(CREW_CODE)
		REFERENCES CREWS(CREW_CODE)
		ON DELETE SET NULL
		ON UPDATE CASCADE;
		
-- Reference Crews -> Crew Types (is_a_CT)
-- Relation 0,1 -> 0,n
-- ALTER TABLE CREWS
-- 	ADD COLUMN (KoCREW_CODE INT),
--  ADD FOREIGN KEY(KoCREW_CODE)
-- 		REFERENCES KoCREW(KoCREW_CODE)
-- 		ON DELETE CASCADE
-- 		ON UPDATE CASCADE;

-- Reference Events -> Event Types (is_a_ET)
ALTER TABLE EVENTS
	ADD COLUMN (KoEVT_CODE INT NOT NULL),
	ADD FOREIGN KEY(KoEVT_CODE)
		REFERENCES KoEVENT(KoEVT_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Events -> StaffMembers (Associate)	
ALTER TABLE EVENTS
	ADD COLUMN (STAFF_CODE INT NOT NULL UNIQUE),
	ADD FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference StaffMembers -> Status (Current_Status)
ALTER TABLE STAFF
	ADD COLUMN (STATUS_CODE SMALLINT NOT NULL),
	ADD FOREIGN KEY (STATUS_CODE)
		REFERENCES STATUS(STATUS_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Managers -> Managed (Manage)
ALTER TABLE STAFF
	ADD COLUMN (MANAGER INT),
	ADD FOREIGN KEY (MANAGER)
		REFERENCES STAFF(STAFF_CODE)
		ON DELETE SET NULL
		ON UPDATE CASCADE;
		
-- Reference Tickets -> Passengers (book)
ALTER TABLE TICKETS
	ADD COLUMN (PSGR_CODE INT NOT NULL),
	ADD FOREIGN KEY (PSGR_CODE)
		REFERENCES PASSENGERS(PSGR_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Tickets -> Currencies (Valorize)
ALTER TABLE TICKETS
	ADD COLUMN (CURR_CODE CHAR(4) NOT NULL),
	ADD FOREIGN KEY (CURR_CODE)
		REFERENCES CURRENCIES(CURR_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Tickets -> Classe (Ticket_class)
ALTER TABLE TICKETS
	ADD COLUMN (CLASS_CODE INT NOT NULL),
	ADD FOREIGN KEY (CLASS_CODE)
		REFERENCES CLASSES(CLASS_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
-- Reference Effective Stopovers -> Planned Stopovers (Match_PS)
ALTER TABLE EFF_STOPOVERS
	ADD COLUMN (STOPOVER_NBR SMALLINT NOT NULL),
	ADD FOREIGN KEY (STOPOVER_NBR)
		REFERENCES PLAN_STOPOVERS(STOPOVER_NBR)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Planned flights -> Airlinks (Travel)	
ALTER TABLE PLAN_FLIGHTS
	ADD COLUMN (AIRLINK_CODE INT NOT NULL),
	ADD FOREIGN KEY (AIRLINK_CODE)
		REFERENCES AIRLINKS(AIRLINK_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- 	Reference Routes -> Airlinks (Match_link)	
ALTER TABLE ROUTES
	ADD COLUMN (AIRLINK_CODE INT NOT NULL),
	ADD FOREIGN KEY (AIRLINK_CODE)
	REFERENCES AIRLINKS(AIRLINK_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Routes -> Aircraft Types (Perorm)
ALTER TABLE ROUTES
	ADD COLUMN (IATA CHAR(4) NOT NULL),
	ADD FOREIGN KEY (IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Airlinks -> Aiports (Start From, Arrive_to)
ALTER TABLE AIRLINKS
	ADD COLUMN (AIRPORT_CODE_FROM CHAR(4) NOT NULL, 
				AIRPORT_CODE_TO CHAR(4) NOT NULL),
	ADD FOREIGN KEY (AIRPORT_CODE_FROM)
		REFERENCES AIRPORTS(AIRPORT_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	ADD FOREIGN KEY (AIRPORT_CODE_TO)
		REFERENCES AIRPORTS(AIRPORT_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
	
-- Reference Assignment -> Staff Members (assign)
ALTER TABLE ASSIGNMENTS
	ADD COLUMN (STAFF_CODE INT NOT NULL),
	ADD FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Assignments -> Roles (with role)
ALTER TABLE ASSIGNMENTS
	ADD COLUMN (ROLE_CODE INT NOT NULL),
	ADD FOREIGN KEY (ROLE_CODE)
		REFERENCES ROLES(ROLE_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Assignment -> Crews (Compose)
ALTER TABLE ASSIGNMENTS
	ADD COLUMN (CREW_CODE INT NOT NULL),
	ADD FOREIGN KEY (CREW_CODE)
		REFERENCES CREWS(CREW_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Effective flights -> Aircrafts (Assign aircraft)
ALTER TABLE EFF_FLIGHTS
	ADD COLUMN (AIRCRAFT_NBR SMALLINT),
	ADD FOREIGN KEY (AIRCRAFT_NBR)
		REFERENCES AIRCRAFTS(AIRCRAFT_NBR)
		ON DELETE SET NULL
		ON UPDATE CASCADE;
		
-- Reference Planned day flights -> Aircraft types (Allocate)
ALTER TABLE PLAN_DAYFLIGHTS
	ADD COLUMN (IATA CHAR(4)),
	ADD FOREIGN KEY (IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON DELETE SET NULL
		ON UPDATE CASCADE;
		
-- Reference Planned Day Flight -> Planned Flights (Plan)
ALTER TABLE PLAN_DAYFLIGHTS
	ADD COLUMN (FLIGHT_NBR SMALLINT NOT NULL),
	ADD FOREIGN KEY (FLIGHT_NBR)
		REFERENCES PLAN_FLIGHTS(FLIGHT_NBR)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Events -> Status (change)
ALTER TABLE EVENTS
	ADD COLUMN (STATUS_CODE SMALLINT),
	ADD FOREIGN KEY (STATUS_CODE)
		REFERENCES STATUS(STATUS_CODE)
		ON DELETE SET NULL
		ON UPDATE CASCADE;

-- Reference Airports -> Currencies (use)
ALTER TABLE AIRPORTS
	ADD COLUMN (CURR_CODE CHAR(4) NOT NULL),
	ADD FOREIGN KEY (CURR_CODE)
		REFERENCES CURRENCIES(CURR_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Day_periods -> Weekdays (is_a_WD)
ALTER TABLE DAY_PERIODS
	ADD COLUMN (DAY_NBR SMALLINT NOT NULL),
	ADD FOREIGN KEY (DAY_NBR)
		REFERENCES WEEKDAYS(DAY_NBR)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Medical checkup -> Staff members (Have_MCK)	
ALTER TABLE CHECKUPS
	ADD COLUMN (STAFF_CODE INT NOT NULL UNIQUE),
	ADD FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Planned day flights -> Weekdays (Plan Day in week)
ALTER TABLE PLAN_DAYFLIGHTS
	ADD COLUMN (DAY_NBR SMALLINT NOT NULL),
	ADD FOREIGN KEY (DAY_NBR)
		REFERENCES WEEKDAYS(DAY_NBR)
		ON DELETE RESTRICT
		ON UPDATE CASCADE;
		
-- Reference Effective Stopovers -> Effective Flights (Include_ES)
ALTER TABLE EFF_STOPOVERS
	ADD COLUMN (EFF_FLIGHTS_FK INT NOT NULL),
	ADD FOREIGN KEY (EFF_FLIGHTS_FK)
		REFERENCES EFF_FLIGHTS(EFF_FLIGHTS_PK)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- Reference Planed Stopovers -> Routes (Finish)
ALTER TABLE PLAN_STOPOVERS
	ADD COLUMN (ROUTE_CODE INT),
	ADD FOREIGN KEY (ROUTE_CODE)
		REFERENCES ROUTES(ROUTE_CODE)
		ON DELETE SET NULL
		ON UPDATE CASCADE;
		
-- Refenreces Effectives Flights -> Planned Day Flights (Tie in)
ALTER TABLE EFF_FLIGHTS
	ADD COLUMN (PLAN_DAYFLIGHTS_FK INT NOT NULL),
	ADD FOREIGN KEY (PLAN_DAYFLIGHTS_FK)
		REFERENCES PLAN_DAYFLIGHTS(PLAN_DAYFLIGHTS_PK)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Refenreces Planned Stopovers -> Planned Day Flights (Include S)
ALTER TABLE PLAN_STOPOVERS
	ADD COLUMN (PLAN_DAYFLIGHTS_FK INT NOT NULL),
	ADD FOREIGN KEY (PLAN_DAYFLIGHTS_FK)
		REFERENCES PLAN_DAYFLIGHTS(PLAN_DAYFLIGHTS_PK)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- References Tickets -> Effctives Flights (Ticket for)
ALTER TABLE TICKETS
	ADD COLUMN (EFF_FLIGHTS_FK INT NOT NULL),
	ADD FOREIGN KEY (EFF_FLIGHTS_FK)
		REFERENCES EFF_FLIGHTS(EFF_FLIGHTS_PK)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		
-- Reference Month_periods -> year_periods (Month_in_year)
ALTER TABLE MONTH_PERIODS
	ADD COLUMN (YEAR_PERIOD CHAR(4) NOT NULL),
	ADD FOREIGN KEY (YEAR_PERIOD)
		REFERENCES YEAR_PERIODS(YEAR_PERIOD)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	ADD PRIMARY KEY (MONTH_PERIOD, YEAR_PERIOD);
	
-- Reference Day periods -> Month periods (Day in month)
ALTER TABLE DAY_PERIODS
	ADD COLUMN (MONTH_PERIOD CHAR(2) NOT NULL),
	ADD COLUMN (YEAR_PERIOD CHAR(4) NOT NULL),
	ADD FOREIGN KEY (MONTH_PERIOD, YEAR_PERIOD)
		REFERENCES MONTH_PERIODS(MONTH_PERIOD, YEAR_PERIOD)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	ADD PRIMARY KEY (DAY_PERIOD, MONTH_PERIOD, YEAR_PERIOD);
	
-- RELATIONS

-- ARRAY TABLES
-- Relation Airport -> Aircraft Types (Accept)
CREATE TABLE IF NOT EXISTS R_ACCEPT (
	AIRPORT_CODE CHAR(4),
	IATA CHAR(4),
	FOREIGN KEY (AIRPORT_CODE)
		REFERENCES AIRPORTS(AIRPORT_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(AIRPORT_CODE, IATA)
);

-- Relation Crew -> Crew Types (is_a_CT)
-- Relation 0,n -> 0,n
CREATE TABLE IF NOT EXISTS R_ISA_KoCREW (
	CREW_CODE INT,
	KoCREW_CODE INT,
	FOREIGN KEY (CREW_CODE)
		REFERENCES CREWS(CREW_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (KoCREW_CODE)
		REFERENCES KoCREW(KoCREW_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(CREW_CODE, KoCREW_CODE)
);

-- Relation Airports -> Cities (Server)
CREATE TABLE IF NOT EXISTS R_SERVE (
	AIRPORT_CODE CHAR(4),
	CITY_CODE VARCHAR(25),
	FOREIGN KEY (AIRPORT_CODE)
		REFERENCES AIRPORTS(AIRPORT_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (CITY_CODE)
		REFERENCES CITIES(CITY_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(AIRPORT_CODE, CITY_CODE)
);

-- Relation AircraftTypes -> Crew Types (Need)
CREATE TABLE IF NOT EXISTS R_NEED (
	IATA CHAR(4),
	KoCREW_CODE INT,
	FOREIGN KEY (IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (KoCREW_CODE)
		REFERENCES KoCREW(KoCREW_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(IATA, KoCREW_CODE)
);

-- Relation Events -> Documents (Bind)
CREATE TABLE IF NOT EXISTS R_BIND (
	EVENT_NBR INT,
	DOC_CODE INT,
	FOREIGN KEY (EVENT_NBR)
		REFERENCES EVENTS(EVENT_NBR)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (DOC_CODE)
		REFERENCES DOC(DOC_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(EVENT_NBR, DOC_CODE)
);

-- Relation Licence Type -> Aircraft Type (Allow_pilot)
CREATE TABLE IF NOT EXISTS R_ALLOW_PILOT (
	KoLIC_CODE INT,
	IATA CHAR(4),
	FOREIGN KEY (KoLIC_CODE)
		REFERENCES KoLICENSE(KoLIC_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY(IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(KoLIC_CODE, IATA)
);

-- Relation Qualifications -> License Types (Require)
CREATE TABLE IF NOT EXISTS R_REQUIRE (
	KoLIC_CODE INT,
	QUALIF_CODE INT,
	FOREIGN KEY (KoLIC_CODE)
		REFERENCES KoLICENSE(KoLIC_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY(QUALIF_CODE)
		REFERENCES QUALIFICATIONS(QUALIF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(KoLIC_CODE, QUALIF_CODE)
);

-- Relation Staff Members -> License Types (Obtain)
CREATE TABLE IF NOT EXISTS R_OBTAIN (
	KoLIC_CODE INT,
	STAFF_CODE INT,
	OBTAINED DATE,
	FOREIGN KEY (KoLIC_CODE)
		REFERENCES KoLICENSE(KoLIC_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(KoLIC_CODE, STAFF_CODE)
);

-- Relation Aircrafts -> Classes (Contain)
CREATE TABLE IF NOT EXISTS R_CONTAIN (
	CLASS_CODE INT,
	AIRCRAFT_NBR SMALLINT,
	SEATS INT,
	FOREIGN KEY (CLASS_CODE)
		REFERENCES CLASSES(CLASS_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (AIRCRAFT_NBR)
		REFERENCES AIRCRAFTS(AIRCRAFT_NBR)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(CLASS_CODE, AIRCRAFT_NBR)
);

-- Relation Staff members -> Qualification (Have)
CREATE TABLE IF NOT EXISTS R_HAVE_QUALIF (
	QUALIF_CODE INT,
	STAFF_CODE INT,
	FOREIGN KEY (QUALIF_CODE)
		REFERENCES QUALIFICATIONS(QUALIF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(QUALIF_CODE, STAFF_CODE)
);

-- Relation Staff members -> Month periods
CREATE TABLE IF NOT EXISTS R_IN_FLIGHT (
	STAFF_CODE INT,
	MONTH_PERIOD CHAR(2),
	YEAR_PERIOD CHAR(4),
	HOURS_NBR SMALLINT,
	FOREIGN KEY (MONTH_PERIOD, YEAR_PERIOD)
		REFERENCES MONTH_PERIODS(MONTH_PERIOD, YEAR_PERIOD)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(STAFF_CODE, MONTH_PERIOD)
);

-- Relation Languages -> Languages (Name_Language)
CREATE TABLE IF NOT EXISTS R_NAME_LANGUAGE (
	LNG_CODE_IN CHAR(2),
	LNG_CODE_OF CHAR(2),
	LNGNAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (LNG_CODE_IN)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (LNG_CODE_OF)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (LNG_CODE_IN, LNG_CODE_OF)
);

-- Relation Weekdays -> Languages (Name_Day)
-- 1,n 0,n relation !
CREATE TABLE IF NOT EXISTS R_NAME_DAY (
	DAY_NBR SMALLINT,
	LNG_CODE CHAR(2),
	DAYNAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	FOREIGN KEY (DAY_NBR)
		REFERENCES WEEKDAYS(DAY_NBR)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (DAY_NBR, LNG_CODE)
);
		
-- Relation Roles -> Languages (Name_Role)
CREATE TABLE IF NOT EXISTS R_NAME_ROLE (
	ROLE_CODE INT,
	LNG_CODE CHAR(2),
	ROLE_NAME VARCHAR(50) NOT NULL,
	ROLE_DSCR TEXT NOT NULL,
	FOREIGN KEY (ROLE_CODE)
		REFERENCES ROLES(ROLE_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (ROLE_CODE, LNG_CODE)
);

-- Relation Aicraft Types -> Languages (Name_AircraftType)
-- 1,n 0,n relation !
CREATE TABLE IF NOT EXISTS R_NAME_KoAIRCRAFT (
	LNG_CODE CHAR(2),
	IATA CHAR(4),
	KoACRNAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (IATA)
		REFERENCES KoAIRCRAFT(IATA)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (LNG_CODE, IATA)
);
		
-- Relation Airports -> Languages (Name Airport)
CREATE TABLE IF NOT EXISTS R_NAME_AIRPORT (
	AIRPORT_CODE CHAR(4),
	LNG_CODE CHAR(2),
	ARP_NAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (AIRPORT_CODE)
		REFERENCES AIRPORTS(AIRPORT_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (AIRPORT_CODE, LNG_CODE)
);

-- Relation Weekdays <-> Timeslots <-> Airlinks (Estimate Passengers)
CREATE TABLE IF NOT EXISTS R_ESTIMATE_NB_PASSENGERS (
	TIMESLOT_CODE INT,
	AIRLINK_CODE INT,
	DAY_NBR SMALLINT,
	ESTIMATED_SEATS SMALLINT NOT NULL,
	FOREIGN KEY (TIMESLOT_CODE)
		REFERENCES TIMESLOTS(TIMESLOT_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (AIRLINK_CODE)
		REFERENCES AIRLINKS(AIRLINK_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (DAY_NBR)
		REFERENCES WEEKDAYS(DAY_NBR)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY(TIMESLOT_CODE, AIRLINK_CODE, DAY_NBR)
);

-- Relation Cities -> Languages (Name_City)
-- 1,n 0,n relation !
CREATE TABLE IF NOT EXISTS R_NAME_CITY (
	CITY_CODE CHAR(25),
	LNG_CODE CHAR(2),
	CITYNAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (CITY_CODE)
		REFERENCES CITIES(CITY_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (CITY_CODE, LNG_CODE)
);

-- Relations Month periods -> Languages (Name_Month)
-- 1,n 0,n relation !
CREATE TABLE IF NOT EXISTS R_NAME_MONTH (
	MONTH_PERIOD CHAR(2),
	LNG_CODE CHAR(2),
	MONTHNAME VARCHAR(50) NOT NULL,
	FOREIGN KEY (LNG_CODE)
		REFERENCES LANGUAGES(LNG_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (MONTH_PERIOD)
		REFERENCES MONTH_PERIODS(MONTH_PERIOD)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (MONTH_PERIOD, LNG_CODE)
);

-- Relation Tickers <-> Currencies <-> Planned Day Flights (Estimate Price)
CREATE TABLE IF NOT EXISTS R_ESTIMATE_PRICE (
	CLASS_CODE INT,
	CURR_CODE CHAR(4),
	PLAN_DAYFLIGHTS_FK INT,
	ESTIMATED_PRICE DECIMAL(19,4),
	FOREIGN KEY (CLASS_CODE)
		REFERENCES CLASSES(CLASS_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (CURR_CODE)
		REFERENCES CURRENCIES(CURR_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (PLAN_DAYFLIGHTS_FK)
		REFERENCES PLAN_DAYFLIGHTS(PLAN_DAYFLIGHTS_PK)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (CLASS_CODE, CURR_CODE, PLAN_DAYFLIGHTS_FK)
);

-- Relation Staff Members <-> Roles (May have)
CREATE TABLE IF NOT EXISTS R_MAY_HAVE (
	STAFF_CODE INT,
	ROLE_CODE INT,
	FOREIGN KEY (STAFF_CODE)
		REFERENCES STAFF(STAFF_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (ROLE_CODE)
		REFERENCES ROLES(ROLE_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (STAFF_CODE, ROLE_CODE)
);

-- Relation Roles <-> Crew types (Include)
CREATE TABLE if NOT EXISTS R_INCLUDE (
	KoCREW_CODE INT,
	ROLE_CODE INT,
	NBR_IN_ROLE SMALLINT,
	FOREIGN KEY (KoCREW_CODE)
		REFERENCES KoCREW(KoCREW_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (ROLE_CODE)
		REFERENCES ROLES(ROLE_CODE)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	PRIMARY KEY (KoCREW_CODE, ROLE_CODE)
);